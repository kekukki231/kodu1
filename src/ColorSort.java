public class ColorSort {

	enum Color {
		red, green, blue
	};

	public static void main(String[] param) {
		// Color[] list = { Color.blue, Color.blue, Color.red, Color.green };
		// //my not so fast reorder method, but made it by myself
		// reorder(list);
		// System.out.print("Ordered: " + Arrays.asList(list));

		// Dutch national flag problem:
		// https://en.wikipedia.org/wiki/Dutch_national_flag_problem
		// Probably fastest reorderer:
		// http://rosettacode.org/wiki/Dutch_national_flag_problem#Java
	}

	public static void reorder(Color[] balls) {
		int l = 0, m = 0, r = 0;

		Color[] newList = new Color[balls.length];

		for (int i = 0; i < balls.length; i++) {
			newList[i] = balls[i];
		}

		for (int i = 0; i < newList.length; i++) {
			if (newList[i].equals(Color.red)) {
				balls[l] = Color.red;
				l++;
			} else if (newList[i].equals(Color.blue)) {
				r++;
				balls[balls.length - r] = Color.blue;
			} else {
				m++;
			}
		}
		for (int j = 0; j <= m - 1; j++) {
			balls[l + j] = Color.green;
		}
	}
}
